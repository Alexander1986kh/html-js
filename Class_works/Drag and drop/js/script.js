/**
 * Created by Саша on 24.03.2017.
 */
function transfer(){
    var coord_start=element.getBoundingClientRect();//Координаты элемента в клиентской области
    var delta_x=event.clientX-coord_start.left;//Разница в координатах между нажатием и объектом
    var delta_y=event.clientY-coord_start.top;//Разница в координатах между нажатием и объектом
    var page_coord_start_x=  coord_start.left+pageXOffset;//Координата элемента с учетом прокрутки(относительно всего документа)
    var page_coord_start_y=  coord_start.top+pageYOffset;//Координата элемента с учетом прокрутки(относительно всего документа)
    document.body.appendChild(element);
    element.style.position='absolute';
    function move(){
        element.style.top=event.pageY-delta_y+'px';
        element.style.left=event.pageX-delta_x+'px';
    }
    document.body.addEventListener('mousemove', move);
    function stop_drag(){
        document.body.removeEventListener('mousemove', move);
        document.body.removeEventListener('mouseup', stop_drag);
    }
    document.body.addEventListener('mouseup', stop_drag);
    // function remove_browser_drag_start(){
    //     event.preventDefault();
    // }
    // element.addEventListener('dragstart', remove_browser_drag_start);
    element.ondragstart = function() {
        return false;
    };
}
element.addEventListener('mousedown', transfer);