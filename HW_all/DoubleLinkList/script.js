function Element(avalue){
    this.value=avalue;
    var prev=null;
    var next=null;
    var self=this;
    // this.value=function(a_set_value)
    // {
    //     if (a_set_value === undefined)
    //         return this.value;
    //     else this.value=a_set_value;
    // };
      this.prev=function(a_prev)
    {
        if (a_prev === undefined)
            return prev;
        else prev=a_prev;
    };
    this.next=function(a_next)
    {
        if (a_next === undefined)
            return next;
        else next=a_next;
    };

    this.self=function(a_self)
    {
        if (a_self === undefined)
            return self;
        else self=a_self;
    }
}
function DLL(){
    var head=new Element();
    var tail=new Element();
    this.head=function(a_set_head_value)
    {
        if (a_set_head_value === undefined)
            return head;
        else head=a_set_head_value;
    };
    this.tail=function(a_set_tail_value){
        if(a_set_tail_value===undefined)
            return tail;
        else
            tail=a_set_tail_value;
    };
    this.add=function(avalue){
        var tmp=new Element(avalue);
        if (head.value != undefined) {

            tmp.prev(tail.self());
            tail.next(tmp.self());
            tail=tmp.self();
        }
        else
        {
            head=tmp.self();
            tail=tmp.self();
        }
    };
    this.show=function(){
       var tmp_el=head;
       while(tmp_el){
           console.log(tmp_el.value);
           tmp_el=tmp_el.next();
       }
};
this.insertByIndex=function(index,aval){
    var insEl= new Element(aval);
    if(index>0){//вставка не в начало
        var prev_=head;
        for(var i=0; i<index - 1 && prev_; i++)//Перехолим на предыдущий для вставки элемент
        prev_ = prev_.next();
        if(prev_){//если пред элемент найден
            var next_=prev_.next();//находим  следущий элемент
            if(!next_){//Если вставка в конец
                tail.next(insEl.self());
                insEl.prev(tail);
                tail=insEl.self();
            }
            else{//если вставка в середину
               prev_.next(insEl.self());
               insEl.prev(prev_);
               next_.prev(insEl.self());
               insEl.next(next_);
                }
            }
        }
    else{//если вставка в начало
        head.prev(insEl.self());
        insEl.next(head);
        head=insEl.self();
    }
};
this.deleteByIndex=function(index)
    {
        if (index == 0) {//удаляем первый элемент
            if (head == tail)
                tail = head = null;
            else {
                head = head.next();
                head.prev(null);
            }
        }
        else {
            var prev_ = head;
            for (var i = 0; i < index - 1 && prev_; i++)
                prev_ = prev_.next();
            if (prev_ && prev_.next()) {//если предыдущий элемент в пределах списка && и есть элемент для удаления
                var next_ = prev_.next();
                next_=next_.next();
                if (!next_) {//удаляем последний элемент
                    tail = prev_;
                    delete tail.next();
                        tail.next(null);

                }
                else {//удаляем элемент из середины
                    prev_.next(next_);
                    next_.prev(prev_);
                }
            }
        }
};
}

var list=new DLL();
list.add(24);
list.add(14);
list.add(64);
list.insertByIndex(0,34);
list.insertByIndex(2,884);
list.insertByIndex(5,3554);
list.insertByIndex(99,3544454);
list.deleteByIndex(5);
list.deleteByIndex(3);
list.deleteByIndex(0);
list.deleteByIndex(99);
list.show();