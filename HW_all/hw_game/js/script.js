/**
 * Created by Саша on 25.03.2017.
 */
function element_pozition_random(){
    element.style.left=Math.floor(Math.random()*(530-0+1)+0)+'px';
    element.style.top=Math.floor(Math.random()*(280-0+1)+0)+'px';
}
function start_game() {
    var span_result = document.querySelector('#result>p>span');
    var span_score = document.querySelector('div.score>span');
    var span_time = document.querySelector('div.time>span');
    start.style.display = 'none';
    result.style.display = 'none';
    element.style.visibility = 'visible'
    element_pozition_random();
    time = setInterval(function () {
        time_left.innerText = String(--total_time);
    }, 1000);
    var time_element = 800;
    var time_game = 30000;
    var score = 0;
    var set_score = document.querySelector('div.score>span');//отображение очков
    var time_left = document.querySelector('div.time>span');//остаток времени
    var total_time = 30;
    var time;

    function hit_in_element() {
        element_pozition_random();
        score++;
        set_score.innerText = String(score);//Преобразование значения в строку
        clearInterval(timerId);
        time_element -= 100;
        timerId = setInterval(function () {
            element_pozition_random();
        }, time_element);
    }

    element.addEventListener('mousedown', hit_in_element);
    var timerId = setInterval(function () {
        element_pozition_random();
    }, time_element);
    var set_result = document.querySelector('#result>p>span');
    setTimeout(function () {
        clearInterval(time);
        clearInterval(timerId);
        time_element = 800;
        element.style.visibility = 'hidden';
        start.style.display = 'block';
        result.style.display = 'block';
        set_result.innerText = String(score);
    }, time_game);
}
start.addEventListener('click', start_game);